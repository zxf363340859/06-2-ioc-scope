package com.twuc.webApp.proxymode;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {

    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private Logger logger;

    public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxy prototypeDependentWithProxy, Logger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        this.logger = logger;
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public Logger getLogger() {
        return logger;
    }

    public void twoCallMethod() {
        prototypeDependentWithProxy.toString();
        prototypeDependentWithProxy.toString();
    }
}
