package com.twuc.webApp.proxymode;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Logger {

    private List<String> list = new ArrayList<>();

    public List<String> getList() {
        return list;
    }

    public void log(String s) {
        list.add(s);
    }
}
