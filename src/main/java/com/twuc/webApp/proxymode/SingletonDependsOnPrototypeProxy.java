package com.twuc.webApp.proxymode;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {

    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private Logger logger;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy, Logger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        this.logger = logger;
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public Logger getLogger() {
        return logger;
    }
}
