package com.twuc.webApp.proxymode;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {

    private Logger logger;

    public PrototypeDependentWithProxy(Logger logger) {
        this.logger = logger;
        logger.log(this.toString());
    }

    public Logger getLogger() {
        return logger;
    }
}
