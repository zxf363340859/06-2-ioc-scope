package com.twuc.webApp.yourTurn;

import com.twuc.webApp.proxymode.PrototypeDependentWithProxy;
import com.twuc.webApp.proxymode.SingletonDependsOnPrototypeProxy;
import com.twuc.webApp.proxymode.SingletonDependsOnPrototypeProxyBatchCall;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class ProxyModeTest {

    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.proxymode");

    @Test
    void test_proxy_create_bean() {
        SingletonDependsOnPrototypeProxy bean1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy bean2 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertNotEquals(bean1.getPrototypeDependentWithProxy().getLogger().getList().get(0), bean1.getPrototypeDependentWithProxy().getLogger().getList().get(1));
    }

    @Test
    void test_proxy_create_bean_when_call_method_twice() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.twoCallMethod();
        assertNotEquals(bean.getLogger().getList().get(0), bean.getLogger().getList().get(1));
    }

    @Test
    void test_proxy_create_bean_is_proxy() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertNotEquals(bean.getPrototypeDependentWithProxy().getClass().getName(), PrototypeDependentWithProxy.class.getName());
    }
}
