package com.twuc.webApp.yourTurn;

import com.twuc.webApp.bean.SingletonBean;
import com.twuc.webApp.bean.SingletonLazyBean;
import com.twuc.webApp.prototypebean.PrototypeBean;
import com.twuc.webApp.prototypebean.PrototypeScopeDependsOnSingleton;
import com.twuc.webApp.prototypebean.SimplePrototypeScopeClass;
import com.twuc.webApp.prototypebean.SingletonDependsOnPrototype;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class PrototypeBeanTest {

    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.prototypebean");
    AnnotationConfigApplicationContext singleContext = new AnnotationConfigApplicationContext();
    AnnotationConfigApplicationContext lazyContext = new AnnotationConfigApplicationContext();

    @Test
    void should_return_two_bean_when_get_bean() {
        assertSame(context.getBean(SimplePrototypeScopeClass.class), context.getBean(SimplePrototypeScopeClass.class));
    }

    // 先注册bean 不调用refreshed 直接获取bean抛异常
    @Test
    void test_container_create_singleton_object_time_1() {
        singleContext.register(SingletonBean.class);
        singleContext.getBean(SingletonBean.class);
    }

    // 调用了refreshed()之后,获得bean，在容易初始化后创建bean
    @Test
    void test_container_create_singleton_object_time_2() {
        singleContext.register(SingletonBean.class);
        singleContext.refresh();
        assertNotNull(singleContext.getBean(SingletonBean.class));
    }

    // 先注册bean 不调用refreshed 直接获取bean抛异常
    @Test
    void test_container_create_prototype_object_time_1() {
        singleContext.register(PrototypeBean.class);
        singleContext.getBean(PrototypeBean.class);
    }

    // 调用了refreshed()之后,获得bean，在容易初始化后创建bean
    @Test
    void test_container_create_prototype_object_time_2() {
        singleContext.register(PrototypeBean.class);
        singleContext.refresh();
        assertNotNull(singleContext.getBean(PrototypeBean.class));
    }

    // 测试singleton懒加载
    @Test
    void test_singleton_bean_lazy_initialization() {
        lazyContext.register(SingletonLazyBean.class);
        lazyContext.refresh();
        assertNotNull(lazyContext.getBean(SingletonLazyBean.class));
    }

    @Test
    void should_return_one_prototype_bean_and_two_singleton_bean() {
        PrototypeScopeDependsOnSingleton prototypeBean1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton prototypeBean2 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertNotSame(prototypeBean1, prototypeBean2);
        assertSame(prototypeBean1.getSingletonDependent(), prototypeBean2.getSingletonDependent());
    }

    @Test
    void should_return_one_singleton_bean_and_two_prototype_bean() {
        SingletonDependsOnPrototype bean1 = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype bean2 = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(bean1, bean2);
        assertSame(bean1.getPrototypeDependent(), bean2.getPrototypeDependent());
    }
}
