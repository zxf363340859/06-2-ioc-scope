package com.twuc.webApp.yourTurn;

import com.twuc.webApp.bean.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class SingletonBeanTest {

    private static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.bean");

    //2,1,1
    @Test
    void should_return_one_bean_when_call_get_bean() {
        assertSame(context.getBean(InterfaceOne.class), context.getBean(InterfaceOneImpl.class));
    }

    //2,1,2 基类和派生类不一样
    @Test
    void should_return_two_bean_when_call_get_super_bean_and_son_bean() {
        assertNotSame(context.getBean(InterfaceOneImpl.class), context.getBean(BigInterfaceOneImpl.class));
    }

    //2,1,3
    @Test
    void should_return_one_bean_when_call_get_bean_in_abstract_class() {
        assertSame(context.getBean(AbstractBaseClass.class), context.getBean(DerivedClass.class));
    }

}
